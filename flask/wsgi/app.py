from datetime import datetime
from flask import Flask
import ptvsd

app = Flask(__name__)

@app.route("/")
def index():
    return 'Hello'

@app.before_first_request
def before_first_request():
    print('### Restarted, first request @ {} ###'.format(datetime.utcnow()))

application = app

try:
    ptvsd.enable_attach()
except OSError as e:
    print(e)
